#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from tucana device
$(call inherit-product, device/xiaomi/tucana/device.mk)

PRODUCT_DEVICE := tucana
PRODUCT_NAME := lineage_tucana
PRODUCT_BRAND := Xiaomi
PRODUCT_MODEL := Mi Note 10
PRODUCT_MANUFACTURER := xiaomi

PRODUCT_GMS_CLIENTID_BASE := android-xiaomi

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="tucana-user 11 RKQ1.200826.002 22.3.31 release-keys"

BUILD_FINGERPRINT := Xiaomi/tucana/tucana:11/RKQ1.200826.002/22.3.31:user/release-keys
