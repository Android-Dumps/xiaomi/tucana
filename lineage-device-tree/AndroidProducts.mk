#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_tucana.mk

COMMON_LUNCH_CHOICES := \
    lineage_tucana-user \
    lineage_tucana-userdebug \
    lineage_tucana-eng
